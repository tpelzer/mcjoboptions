#based on 361035
evgenConfig.description = "Low-pT inelastic minimum bias events for pile-up, with the A2 MSTW2008LO tune and EvtGen"
evgenConfig.keywords = ["QCD", "minBias", "SM"]

evgenConfig.saveJets = True

include("Pythia8_i/Pythia8_A2_MSTW2008LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += \
    ["SoftQCD:inelastic = on"]

#include("GeneratorFilters/AntiKt4TruthJets_pileup.py")
#include("GeneratorFilters/AntiKt6TruthJets_pileup.py")

include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6)
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)

from AthenaCommon.SystemOfUnits import GeV
filtSeq.QCDTruthJetFilter.MinPt = 35.*GeV

evgenConfig.minevents = 100


