##################################################################################
# Job options for Pythia8B_i generation of Lambda_b-bar->PQ0(->J/psi(mumu)ppi)K0_S
##################################################################################
evgenConfig.description = "Signal Lambda_b-bar->PQ0(->J/psi(mumu)ppi)K0_S"
evgenConfig.keywords = ["bottom","exclusive","Lambda_b0","Jpsi","2muon"]
evgenConfig.contact = [ 'gladilin@mail.cern.ch' ]
evgenConfig.process = "pp->bb->Lambda_b-bar->PQ0(->J/psi(mumu)ppi)K0_S"
evgenConfig.nEventsPerJob = 100

#include("Pythia8B_i/Pythia8B_A14_CTEQ6L1_Common.py")
include("Pythia8B_i/Pythia8B_A14_NNPDF23LO_Common.py")

# Hard process
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']
#
genSeq.Pythia8B.VetoDoubleBEvents = True

#
# Event selection
#
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 9.'] 
genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 9.0
genSeq.Pythia8B.QuarkEtaCut = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 3.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

#
# J/psi:
#
genSeq.Pythia8B.Commands += ['443:m0 = 3.096900']  # PDG 2017/2019
genSeq.Pythia8B.Commands += ['443:mWidth = 0.0000929'] # PDG 2017/2019
#
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']

#
# K_S0:
#
genSeq.Pythia8B.Commands += ['310:onMode = off']
genSeq.Pythia8B.Commands += ['310:0:onMode = on']

#
# Lambda_b:
#
genSeq.Pythia8B.Commands += ['5122:m0 = 5.61960']  # PDG 2019
genSeq.Pythia8B.Commands += ['5122:tau0 = 0.4410'] # PDG 2019
#
# Lambda_b decays:
#
genSeq.Pythia8B.Commands += ['5122:onMode = 2']
#
genSeq.Pythia8B.Commands += ['5122:addChannel = 3 0.2 0 441122 310']
genSeq.Pythia8B.Commands += ['5122:addChannel = 3 0.5 0 441124 310']
genSeq.Pythia8B.Commands += ['5122:addChannel = 3 0.3 0 441126 310']

#
# P_c(4312)0, with LHCb mass and width for P_c(4312)+
#
genSeq.Pythia8B.Commands += ['441122:new = Pc(4312)0 Pc(4312)bar0 1 0 0 4.3119 0.0098 4.2 5.1 0.']
genSeq.Pythia8B.Commands += ['441122:addChannel = 1 1. 0 443 2212 -211']

#
# P_c(4440)0, with LHCb mass and width for P_c(4440)+
#
genSeq.Pythia8B.Commands += ['441124:new = Pc(4440)0 Pc(4440)bar0 1 0 0 4.4403 0.0206 4.2 5.1 0.']
genSeq.Pythia8B.Commands += ['441124:addChannel = 1 1. 0 443 2212 -211']

#
# P_c(4457)0, with LHCb mass and width for P_c(4457)+
#
genSeq.Pythia8B.Commands += ['441126:new = Pc(4457)0 Pc(4457)bar0 1 0 0 4.4573 0.0064 4.2 5.1 0.']
genSeq.Pythia8B.Commands += ['441126:addChannel = 1 1. 0 443 2212 -211']

genSeq.Pythia8B.SignalPDGCodes = [443,-13,13]
genSeq.Pythia8B.SignalPtCuts = [0.,3.5,3.5]
genSeq.Pythia8B.SignalEtaCuts = [100.,2.5,2.5]

genSeq.Pythia8B.OutputLevel = INFO
genSeq.Pythia8B.NHadronizationLoops = 4

include("GeneratorFilters/ParentChildwStatusFilter.py")
filtSeq.ParentChildwStatusFilter.PDGParent  = [5122]
filtSeq.ParentChildwStatusFilter.StatusParent  = [2]
filtSeq.ParentChildwStatusFilter.PtMinParent =  11000.
filtSeq.ParentChildwStatusFilter.EtaRangeParent = 2.4
filtSeq.ParentChildwStatusFilter.PDGChild = [310]
filtSeq.ParentChildwStatusFilter.PtMinChild = 800.
filtSeq.ParentChildwStatusFilter.EtaRangeChild = 2.5
