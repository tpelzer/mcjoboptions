#!/bin/bash

# Make sure that if a command fails the script will continue
set +e

unique=true

# Get the files modified in the last commit
modified=($(git diff-tree --name-only -r origin/master..HEAD --diff-filter=A | grep -E "mc.*py"))

if [[ "${#modified[@]}" > 0 ]] ; then
  echo "jO files modified in the latest commit: ${#modified[@]}"
else
  echo "No jO files modified since last commit"
  exit 0
fi

# Loop over modified files
for file in "${modified[@]}" ; do
  # Get physics short
  physicsShort=$(echo $file | awk 'BEGIN {FS="/"} ; END {print $NF}' | sed -E "s/mc\.//" | sed "s/\.py//")
  echo "Checking $file : physicsShort = $physicsShort"
  # Find how many files have same physics short
  nfiles=$(find . -name "mc.$physicsShort.py" | wc -l)
  if (( $nfiles > 1 )) ; then
    unique=false
    echo "Duplicate physicsShort found:"
    find . -name "mc.$physicsShort.py"
  fi
done

if [ "$unique" = true ] ; then
  echo "Result: SUCCESS"
  exit 0
else
  echo "Result: FAILURE"
  exit 1
fi
